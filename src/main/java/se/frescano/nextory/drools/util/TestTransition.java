package se.frescano.nextory.drools.util;

import java.io.File;

import org.kie.api.io.Resource;
import org.kie.api.runtime.KieSession;
import org.kie.internal.io.ResourceFactory;

import se.frescano.nextory.drools.bean.StateMigrationVO;

public class TestTransition {

	/*public static void main(String[] args) {
		
		//testTransition();
		
		testMigration();
	}*/

	//Code not used, so commented
	/*private static void testTransition() {
		Resource resource = ResourceFactory.newFileResource(new File("C:\\temp\\decision-table"+File.separator+"MTR2.xlsx"));	
		KieSession kSession = new DroolsBeanFactory().getKieSession(resource);		
		 MemberStateVO transitionBean=new MemberStateVO();
		 transitionBean.setCurrentState(MemberStateEnum.FREE_TRIAL.name());
		 transitionBean.setPreviousState(MemberStateEnum.FREE_TRIAL.name()); 
		 transitionBean.setSystemEvent(SystemEvent.PARKED.name());
		 transitionBean.setTrigger(TriggerEnum.CHARGE.name());	
		 transitionBean.setResult(ResultEnum.SUCCESS.name());
		 transitionBean = new NextoryStateTransitionEngine().executeTransitionRules(kSession,transitionBean);	
		 System.out.println("currentState::"+transitionBean.getCurrentState());
		 System.out.println("previousState::"+transitionBean.getPreviousState());
		 System.out.println("UserEvent::"+transitionBean.getUserEvent());
		 System.out.println("SystemEvent::"+transitionBean.getSystemEvent());
		 System.out.println("Trigger::"+transitionBean.getTrigger());
		 System.out.println("Result::"+transitionBean.getResult());
		 System.out.println("NewCurrentState::"+transitionBean.getNewCurrentState());
		 System.out.println("NewPreviousState::"+transitionBean.getNewPreviousState());
		 System.out.println("NewUserEvent::"+transitionBean.getNewUserEvent());
		 System.out.println("NewSystemEvent::"+transitionBean.getNewSystemEvent());
	}*/
	
	/*private static void testMigration() {
		 Resource resource = ResourceFactory.newFileResource(new File("C:\\temp\\decision-table"+File.separator+"MTMigration1.xlsx"));
		 KieSession kSession = new DroolsBeanFactory().getKieSession(resource);		
		 StateMigrationVO stateMigVo=new StateMigrationVO();
		 stateMigVo.setMemberType("FREE_TRIAL_PARKED");
		 stateMigVo.setPrevMemType("FREE_TRIAL_MEMBER");

		 stateMigVo = new NextoryStateTransitionEngine().executeStateMigrationRules(kSession,stateMigVo);
		 
		 System.out.println("MemberType::"+stateMigVo.getMemberType());
		 System.out.println("PrevMemType::"+stateMigVo.getPrevMemType());
		 
		 System.out.println("currentState::"+stateMigVo.getCurrentState());
		 System.out.println("previousState::"+stateMigVo.getPreviousState());
	
		 System.out.println("UserEvent::"+stateMigVo.getUserEvent());
		 System.out.println("SystemEvent ::"+ stateMigVo.getSystemEvent());

	}*/

}
