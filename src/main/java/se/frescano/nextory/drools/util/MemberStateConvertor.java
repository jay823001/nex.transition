package se.frescano.nextory.drools.util;

public class MemberStateConvertor {/*
	
	private static LinkedHashMap<String,String> stateConvertionMap=new LinkedHashMap<>();
	
	private static String PAYING_MEMBER="MEMBER_PAYING";
	
	//public static void doConvertMemberStateMap(){
	
	static{
		
	
		//Visitor Types
		stateConvertionMap.put(VISITOR_NO_ACTION.name(), VISITOR.name());
		stateConvertionMap.put(VISITOR_GIFTCARD_BUYER.name(), VISITOR_GIFTCARD.name());
		
		//Free Types
		stateConvertionMap.put(FREE_TRIAL_MEMBER.name(), FREE_TRIAL.name());
		stateConvertionMap.put(FREE_GIFTCARD_MEMBER.name(), FREE_GIFTCARD.name());
		stateConvertionMap.put(FREE_GIFTCARD_NOCARDINFO.name(), FREE_GIFTCARD.name());
		stateConvertionMap.put(FREE_GIFTCARD_UPGRADE_MEMBER.name(), FREE_GIFTCARD.name());
		stateConvertionMap.put(FREE_CAMPAIGN_MEMBER.name(), FREE_CAMPAIGN.name());
		
		
		//Member Types
		stateConvertionMap.put(PAYING_MEMBER, PAYING_MEMBER);
		stateConvertionMap.put(MEMBER_GIFTCARD_EXISTING.name(), MEMBER_GIFTCARD.name());
		stateConvertionMap.put(MEMBER_CAMPAIGN_EXISTING.name(), MEMBER_CAMPAIGN.name());
		stateConvertionMap.put(MEMBER_DISCOUNTED_PRICE_CAMPAIGN.name(), MEMBER_DISCOUNTCAMPAIGN.name());
		stateConvertionMap.put(MEMBER_FIXED_PRICE_CAMPAIGN.name(), MEMBER_FIXEDCAMPAIGN.name());
		
		
		
		FREE_TRIAL_MEMBER			(203002,new Integer []{103002,203004,403002,403004},203002,true ,false ),
		FREE_GIFTCARD_NOCARDINFO	(201002,new Integer []{101002,201004,401002},201002,true ,false ),
		FREE_CAMPAIGN_MEMBER		(202002,new Integer []{102002, 202004, 402002},202002,true ,false ),
		FREE_GIFTCARD_MEMBER		(201007,null,201007,true ,false ),
		FREE_GIFTCARD_UPGRADE_MEMBER(201010,null,null,true ,false ),
		FREE_GIFTCARD_CANCELLED		(201003,null,null,true ,false),
		FREE_CAMPAIGN_CANCELLED		(202003,null,null,true ,false ),
		FREE_CARD_EXPRIYDUE			(205006,new Integer []{201006,202006,203006},null,true ,false ),
		FREE_TRANSACTION_FAILED     (203011,new Integer []{202011,201011,203011},null,true,false),
		
		FREE_TRIAL_PARKED(203014,new Integer []{203014,203021},203014,true,false),
	    FREE_TRIAL_CANCELLED_PARKED(203017,null,203017,true,false),
	    FREE_CAMPAIGN_PARKED(202014,new Integer []{202014,202021},202014,true,false),
	    FREE_CAMPAIGN_CANCELLED_PARKED(202017,null,202017,true,false),
	    FREE_GIFTCARD_PARKED(201014,new Integer []{201014,201021},201014,true,false),
	    FREE_GIFTCARD_CANCELLED_PARKED(201017,null,201017,true,false),
	    FREE_TRANSACTION_FAILED_PARKED(203025,new Integer []{203025,203032},203025,true,false),
	    FREE_TRANSACTION_FAILED_CANCELLED_PARKED(203028,null,203028,true,false),
		
		
		MEMBER_PAYING				(304001,new Integer []{201008, 203008,202008, 301008,302008,304008,404004,304004,404008,404002},304001,true ,false ),
		MEMBER_CARD_EXPIRYDUE		(305006,new Integer []{304006,301006,302006,205008},null,true ,false ),
		
		MEMBER_GIFTCARD_EXISTING	(301002,new Integer []{301004},304001,true ,false ),
		MEMBER_CAMPAIGN_EXISTING	(302002,new Integer []{302004},304001,true ,false ),
		MEMBER_EMPLOYEE				(300000,null,null,true ,false ),

		MEMBER_PAYING_CANCELLED		(304003,new Integer []{304003},null,true ,false ),
		MEMBER_GIFTCARD_CANCELLED 	(301003,null,null,true ,false ),
		MEMBER_CAMPAIGN_CANCELLED	(302003,null,null,true ,false ),
		MEMBER_TRANSACTION_FAILED   (304011,new Integer []{301011,302011,304011,305011},null,true,false),
		MEMBER_FIXED_PRICE_CAMPAIGN (302012,new Integer []{302012,403012,401012,402012,404012,405012,102012,202012,203012},null,true,false),
		MEMBER_DISCOUNTED_PRICE_CAMPAIGN(302013,new Integer []{302013,403013,401013,402013,404013,405013,102013,202013,203013},null,true,false),
		MEMBER_FIXED_CAMPAIGN_TRANSACTION_FAILED(302014,new Integer []{302023},302014,true,false),
		MEMBER_DISCOUNTED_CAMPAIGN_TRANSACTION_FAILED(302015,new Integer []{302024},302015,true,false),
		
		MEMBER_PAYING_PARKED(304014,new Integer []{304014,304021},304014,true,false),
		MEMBER_PAYING_CANCELLED_PARKED(304017,null,304017,true,false),
		MEMBER_TRANSACTION_FAILED_PARKED(304025,new Integer []{304025,304032},304025,true,false),	
		MEMBER_FIXED_PRICE_CAMPAIGN_PARKED(302026,null,304026,true,false),
		MEMBER_DISCOUNTED_PRICE_CAMPAIGN_PARKED(302027,null,304027,true,false),
		MEMBER_TRANSACTION_FAILED_CANCELLED_PARKED(304028,null,304028,true,false),
		MEMBER_FIXED_CAMPAIGN_TRANSACTION_FAILED_PARKED(302028,new Integer []{302028},302028,true,false),
		MEMBER_DISCOUNTED_CAMPAIGN_TRANSACTION_FAILED_PARKED(302029,new Integer []{302029},302029,true,false),
		MEMBER_GIFTCARD_EXISTING_PARKED(301016,new Integer []{301016},301016,true ,false ),
		MEMBER_CAMPAIGN_EXISTING_PARKED(302016,new Integer []{302016},302016,true ,false ),
		
		
		NONMEMBER_PREVIOUS_TRIAL	(403005,new Integer []{203005,203003},null,false,true ),
		NONMEMBER_PREVIOUS_GIFTCARD	(401005,new Integer []{201005},null,false,true ),
		NONMEMBER_PREVIOUS_CAMPAIGN	(402005,new Integer []{202005},null,false,true ),
		NONMEMBER_PREVIOUS_MEMBER	(404005,new Integer []{301005,302005,304005},304001,false,true ),
		NONMEMBER_CARD_EXPIRED		(405006,new Integer []{205005,305005},null,false,true ),
		NONMEMBER_TRANSACTION_FAILED(404011,new Integer []{304016,302019,302020},null,false,true);
		
	}	
	public static String getConvertedState(String currentMemberType){
		String convertedState=stateConvertionMap.get(currentMemberType);
		if(StringUtils.isNotEmpty(convertedState))
			return convertedState;
		else return currentMemberType;
	}
	
	public static LinkedHashMap<String, String> getStateConvertionMap() {
		return stateConvertionMap;
	}

	public static void setStateConvertionMap(LinkedHashMap<String, String> stateConvertionMap) {
		MemberStateConvertor.stateConvertionMap = stateConvertionMap;
	}
	public static void main(String[] args) {
		System.out.println(MemberStateConvertor.getConvertedState(VISITOR_NO_ACTION.toString()));
		System.out.println(MemberStateConvertor.getConvertedState(VISITOR_GIFTCARD_BUYER.toString()));
		System.out.println(MemberStateConvertor.getConvertedState(FREE_TRIAL_MEMBER.toString()));
		System.out.println(MemberStateConvertor.getConvertedState(FREE_GIFTCARD_MEMBER.toString()));
		System.out.println(MemberStateConvertor.getConvertedState(FREE_GIFTCARD_NOCARDINFO.toString()));
		System.out.println(MemberStateConvertor.getConvertedState(FREE_GIFTCARD_UPGRADE_MEMBER.toString()));
		System.out.println(MemberStateConvertor.getConvertedState(FREE_CAMPAIGN_MEMBER.toString()));
		System.out.println(MemberStateConvertor.getConvertedState(PAYING_MEMBER));
		System.out.println(MemberStateConvertor.getConvertedState(MEMBER_GIFTCARD_EXISTING.toString()));
		System.out.println(MemberStateConvertor.getConvertedState(MEMBER_CAMPAIGN_EXISTING.toString()));
		System.out.println(MemberStateConvertor.getConvertedState(MEMBER_DISCOUNTED_PRICE_CAMPAIGN.toString()));
		System.out.println(MemberStateConvertor.getConvertedState(MEMBER_FIXED_PRICE_CAMPAIGN.toString()));
		for(String key: MemberStateConvertor.stateConvertionMap.keySet()){
			System.out.println(key);
		}
	}
*/}
