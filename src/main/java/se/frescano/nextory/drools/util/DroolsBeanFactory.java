package se.frescano.nextory.drools.util;

import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieRepository;
import org.kie.api.builder.ReleaseId;
import org.kie.api.io.Resource;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

public class DroolsBeanFactory {

	private KieServices kieServices = KieServices.Factory.get();

	public KieSession getKieSession(KieContainer kieContainer,Resource dt) {
		KieSession ksession = kieContainer.newKieSession();
		return ksession;
	}

	
	public KieContainer getKieContainer(Resource dt) {
		KieFileSystem kieFileSystem = kieServices.newKieFileSystem().write(dt);

		@SuppressWarnings("unused")
		KieBuilder kieBuilder = kieServices.newKieBuilder(kieFileSystem).buildAll();

		KieRepository kieRepository = kieServices.getRepository();

		ReleaseId krDefaultReleaseId = kieRepository.getDefaultReleaseId();

		return  kieServices.newKieContainer(krDefaultReleaseId);
	}
	
	
/*	public static KieSession getStateMigrationSessionInstance() {

		if (kSession == null) {
			Resource resource = ResourceFactory.newClassPathResource("se/frescano/nextory/drools/rules/OldToNewMigration.xls", DroolsBeanFactory.class);
			//Resource resource = ResourceFactory.newFileResource("D:\\temp\\TransitionRules.xls");
			kSession = new DroolsBeanFactory().getKieSession(resource);			
		}
		//MemberStateConvertor.doConvertMemberStateMap();
		return kSession;
	}
*/

}
