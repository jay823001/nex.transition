package se.frescano.nextory.drools.util;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.drools.core.io.impl.ClassPathResource;
import org.kie.api.event.rule.AgendaEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import se.frescano.nextory.drools.bean.MemberStateVO;
import se.frescano.nextory.drools.bean.StateMigrationVO;
import se.frescano.nextory.drools.listener.TrackingRuleFiredEventListener;

@Service("nextoryStateTransitionEngine")
public class NextoryStateTransitionEngine {

	private static String TRANSITIONS_DECISION_TABLE_CURRENT_VERSION = "MemberTransitions_V_1_0.xlsx";

	KieContainer kieContainer;
	@PostConstruct
	public void initialize() {
		
		if(kieContainer==null){
			logger.info("getKieContainer :: Start");
				kieContainer = getKieContainer(TRANSITIONS_DECISION_TABLE_CURRENT_VERSION);
			logger.info("getKieContainer :: End");
		}
			
	}
	

	private KieContainer getKieContainer(String decisionTable) {
		 return new DroolsBeanFactory().getKieContainer(new ClassPathResource(decisionTable));
	}


	private static final Logger logger = LoggerFactory.getLogger(NextoryStateTransitionEngine.class);
	
	public MemberStateVO executeTransitionRules(MemberStateVO memberStateVO){   

	//	logger.info("initializing transition session");
		KieSession transitionSession = getTransitionSession(kieContainer,TRANSITIONS_DECISION_TABLE_CURRENT_VERSION);
		
		if(StringUtils.isBlank(memberStateVO.getUserEvent())) memberStateVO.setUserEvent("NA");
		if(StringUtils.isBlank(memberStateVO.getSystemEvent())) memberStateVO.setSystemEvent("NA");
		if(StringUtils.isBlank(memberStateVO.getTrigger())) memberStateVO.setTrigger("NA");
		if(StringUtils.isBlank(memberStateVO.getResult())) memberStateVO.setResult("NA");
		
		FactHandle handle = transitionSession.insert(memberStateVO);
		
		AgendaEventListener listner = new TrackingRuleFiredEventListener();
		transitionSession.addEventListener(listner);

		memberStateVO.setRulesExecutedCount(transitionSession.fireAllRules());
		

        for(String ruleName : ((TrackingRuleFiredEventListener)listner).getActivationList())  logger.info("Rule Fired:"+ruleName);
        
        transitionSession.delete(handle);
    //    logger.info("Calling disponse on  transition session");
        transitionSession.dispose();
		return memberStateVO;
}
	
	/**
	 * This method will do the migration from ancient style of current and previous member types to modern style of current and previous member types
	 * @param migrationRuleSession
	 * @param stateMigrationVO
	 */
	public StateMigrationVO executeStateMigrationRules(KieSession migrationRuleSession,StateMigrationVO stateMigrationVO){
			 FactHandle handle = migrationRuleSession.insert(stateMigrationVO);
			 int count = migrationRuleSession.fireAllRules();	
			 if(count == 0)
				 logger.info("Rule not found for old types:(prev-current)"+stateMigrationVO.getPrevMemType()+"-"+stateMigrationVO.getMemberType());
			 migrationRuleSession.delete(handle);
			 return stateMigrationVO;
	}
	
/*	public static KieSession getMigrationRulesSession(){
		KieSession migrationRuleSession = DroolsBeanFactory.getStateMigrationSessionInstance(); 
		return migrationRuleSession;
	}
	
	public static KieSession getTransitionSession(){
		KieSession transitionRulesSession = DroolsBeanFactory.getTransitionRulesSessionInstance();
		return transitionRulesSession;
	}*/
	public static KieSession getTransitionSession(KieContainer kieContainer,String decisionTable){
		return new DroolsBeanFactory().getKieSession(kieContainer,new ClassPathResource(decisionTable));
	}
}
