package se.frescano.nextory.drools.listener;

import java.util.ArrayList;
import java.util.List;

import org.kie.api.event.rule.AfterMatchFiredEvent;
import org.kie.api.event.rule.DefaultAgendaEventListener;
import org.kie.api.runtime.rule.Match;


public class TrackingRuleFiredEventListener extends DefaultAgendaEventListener{
private List<String> activationList = new ArrayList<String>();

public void afterMatchFired(AfterMatchFiredEvent event) {
    Match match = event.getMatch();
    String ruleName = match.getRule().getName();
    activationList.add(ruleName);   
}
public boolean isRuleFired(String ruleName) {
    for (String rule : activationList) {
        if (ruleName.equalsIgnoreCase(rule)) {
            return true;
        }
    }
    return false;
}
public void reset() {
    activationList.clear();
}
public final List<String> getActivationList() {
    return activationList;
} 
}
