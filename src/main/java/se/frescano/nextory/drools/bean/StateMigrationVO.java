package se.frescano.nextory.drools.bean;

public class StateMigrationVO {
	
	private String memberType;
	private String prevMemType;
	private String currentState;
	private String previousState;
	private String userEvent;
	private String systemEvent;
	
	
	public String getMemberType() {
		return memberType;
	}
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}
	public String getPrevMemType() {
		return prevMemType;
	}
	public void setPrevMemType(String prevMemType) {
		this.prevMemType = prevMemType;
	}
	public String getCurrentState() {
		return currentState;
	}
	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}
	public String getPreviousState() {
		return previousState;
	}
	public void setPreviousState(String previousState) {
		this.previousState = previousState;
	}
	public String getUserEvent() {
		return userEvent;
	}
	public void setUserEvent(String userEvent) {
		this.userEvent = userEvent;
	}
	public String getSystemEvent() {
		return systemEvent;
	}
	public void setSystemEvent(String systemEvent) {
		this.systemEvent = systemEvent;
	}
	
	
}
