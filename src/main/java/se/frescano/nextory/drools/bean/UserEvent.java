package se.frescano.nextory.drools.bean;

import org.apache.commons.lang.StringUtils;

public enum UserEvent {	
	NA,
	PURCHASE,
	CARDUPDATE,
	G_REDEEM,
	C_REDEEM,	
	F_REDEEM,
	D_REDEEM,
	CANCELLED,
	CHARGE;

	public static boolean isCancelledMember(UserEvent userEvent) {		
		if(userEvent!=null && userEvent == UserEvent.CANCELLED) return true;
		else return false;
	}
	
	public static UserEvent getUserEvent(String userEvent){
		return StringUtils.isNotBlank(userEvent)?UserEvent.valueOf(userEvent):null;
	}
}

