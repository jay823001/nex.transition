package se.frescano.nextory.drools.bean;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public enum MemberStateEnum {

     EMPLOYEE(true,false,false,true),	
     VISITOR(false,false,false,false),
     VISITOR_GIFTCARD(false,false,false,false),
     FREE_TRIAL(true,false,true,false),
     FREE_CAMPAIGN(true,false,true,false),
     FREE_GIFTCARD(true,false,true,false),
     MEMBER_PAYING(true,false,false,true),
     MEMBER_CAMPAIGN(true,false,false,true),
     MEMBER_GIFTCARD(true,false,false,true),
     MEMBER_FIXEDCAMPAIGN(true,false,false,true),
     MEMBER_DISCOUNTCAMPAIGN(true,false,false,true),
     NONMEMBER(false,true,false,false);
     
     
	private boolean activeMember ;
	private boolean oldMember;
	private boolean isFreeMemberGroup;
	private boolean isPayingMemberGroup;
	
	
	private MemberStateEnum(boolean activeMember, boolean oldMember,boolean isFreeMemberGroup,boolean isPayingMemberGroup) {		
		this.activeMember = activeMember;
		this.oldMember = oldMember;
		this.isFreeMemberGroup = isFreeMemberGroup;
		this.isPayingMemberGroup = isPayingMemberGroup;
	}
	
	public boolean isActiveMember() {
		return activeMember;
	}

	public void setActiveMember(boolean activeMember) {
		this.activeMember = activeMember;
	}
	
	public boolean isOldMember() {
		return oldMember;
	}

	public void setOldMember(boolean oldMember) {
		this.oldMember = oldMember;
	}
	
	public boolean isFreeMemberGroup() {
		return isFreeMemberGroup;
	}

	public void setFreeMemberGroup(boolean isFreeMemberGroup) {
		this.isFreeMemberGroup = isFreeMemberGroup;
	}

	public boolean isPayingMemberGroup() {
		return isPayingMemberGroup;
	}

	public void setPayingMemberGroup(boolean isPayingMemberGroup) {
		this.isPayingMemberGroup = isPayingMemberGroup;
	}

	/**
	 *  This function verifies whether user is an active member or not
	 * @param currentState
	 * @return
	 */
	
	public static boolean checkIsActiveMember(String currentState){
		if(StringUtils.isNotBlank(currentState)){
			return MemberStateEnum.valueOf(currentState).isActiveMember();
		}
		return false;	
	}
	
	public static MemberStateEnum getMemberState(String memberState){
		return StringUtils.isNotBlank(memberState)?MemberStateEnum.valueOf(memberState):null;
	}
	
	public static boolean isVisitorGroup(String memberState){
		boolean isVisitorGroup=false;
		if(StringUtils.isNotBlank(memberState)){
			MemberStateEnum state = MemberStateEnum.valueOf(memberState);
			if(!state.isActiveMember() && !state.isOldMember())
				return true;
		}
		return isVisitorGroup;
	}

	public static boolean isFreeGiftcardNoCardInfo(String currentState, SystemEvent systemEvent) {
		if(StringUtils.isNotBlank(currentState) && MemberStateEnum.getMemberState(currentState) == MemberStateEnum.FREE_GIFTCARD &&
				systemEvent == SystemEvent.NOCARDINFO) return true;
		return false;
	}
	
	public static boolean isOldMember(String memberState){
		
		if(StringUtils.isNotBlank(memberState)){
			return MemberStateEnum.getMemberState(memberState).isOldMember();
		}
		return false;
	}

	public static boolean isGiftCardState(String memberState) {
		
		if(StringUtils.isNotBlank(memberState) && (MemberStateEnum.getMemberState(memberState) == MemberStateEnum.FREE_GIFTCARD || 
				MemberStateEnum.getMemberState(memberState) == MemberStateEnum.MEMBER_GIFTCARD)){
			return true;
		}
		return false;
	}

	public static boolean isVisitorGiftcard(String memberState) {
		
		if(StringUtils.isNotBlank(memberState) && MemberStateEnum.getMemberState(memberState) == MemberStateEnum.VISITOR_GIFTCARD){
			return true;
		}
		return false;
	}
	

	public static boolean isDicountCampaign(String memberState) {
		
		if(StringUtils.isNotBlank(memberState) && MemberStateEnum.getMemberState(memberState) == MemberStateEnum.MEMBER_DISCOUNTCAMPAIGN){
			return true;
		}
		return false;
	}
	public static boolean isFixedCampaign(String memberState) {
		
		if(StringUtils.isNotBlank(memberState) && MemberStateEnum.getMemberState(memberState) == MemberStateEnum.MEMBER_FIXEDCAMPAIGN){
			return true;
		}
		return false;
	}
	public static boolean isCampaignGroup(String memberState) {
		
		if(StringUtils.isNotBlank(memberState))
		{
			MemberStateEnum state = MemberStateEnum.getMemberState(memberState);
			if(state == MemberStateEnum.FREE_CAMPAIGN || state == MemberStateEnum.MEMBER_DISCOUNTCAMPAIGN
					|| state == MemberStateEnum.MEMBER_FIXEDCAMPAIGN || state == MemberStateEnum.MEMBER_CAMPAIGN)
				return true;
			
		}
		return false;
	}
	
	public static List<String> getActiveMemberStates(){
		List<String> activeList = new ArrayList<>();
		for(MemberStateEnum state: MemberStateEnum.values()){
			if(state.isActiveMember())
				activeList.add(state.name());
		}
		return activeList;
	}
}
