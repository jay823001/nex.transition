package se.frescano.nextory.drools.bean;

import org.apache.commons.lang.StringUtils;

public enum SystemEvent {
	NA,
	EXPIRYDUE, 
	NOCARDINFO, 
	PARKED;

	public static boolean isParkedState(SystemEvent systemEvent) {		
		if(systemEvent!=null && systemEvent == SystemEvent.PARKED) return true;
		else return false;
	}
	
	public static SystemEvent getSystemEvent(String systemEvent){
		return StringUtils.isNotBlank(systemEvent)?SystemEvent.valueOf(systemEvent):null;
	}
}
