package se.frescano.nextory.drools.bean;

public enum TriggerEnum {    
	AUTHORIZATION,
    PURCHASE,    
    C_REDEEM,    
    D_REDEEM,
    F_REDEEM,
    G_REDEEM,
    CHARGE,
    FINAL_CHARGE,
    CANCELLED, 
}
