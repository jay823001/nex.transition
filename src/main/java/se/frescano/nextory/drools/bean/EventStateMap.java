package se.frescano.nextory.drools.bean;


public class EventStateMap {


private String  eventname;
private Boolean isuserevent;
private Boolean issystemevent;
private Boolean iscancelled;
private Boolean isparked;
private Boolean isnocardinfo;
private Boolean isexpirydue;
private Boolean ischargefailed;
private Boolean isgiftredeem;
private Boolean iscampaignredeem;
private Boolean isfixedcampredeem;
private Boolean isdiscountcampredeem;
private Boolean iscardupdate;
private Boolean ispurchase;
private Boolean isnoevent;


public Boolean getIsnoevent() {
	return isnoevent;
}

public void setIsnoevent(Boolean isnoevent) {
	this.isnoevent = isnoevent;
}

public String getEventname() {
return eventname;
}

public void setEventname(String eventname) {
this.eventname = eventname;
}

public Boolean getIsuserevent() {
return isuserevent;
}

public void setIsuserevent(Boolean isuserevent) {
this.isuserevent = isuserevent;
}

public Boolean getIssystemevent() {
return issystemevent;
}

public void setIssystemevent(Boolean issystemevent) {
this.issystemevent = issystemevent;
}

public Boolean getIscancelled() {
return iscancelled;
}

public void setIscancelled(Boolean iscancelled) {
this.iscancelled = iscancelled;
}

public Boolean getIsparked() {
return isparked;
}

public void setIsparked(Boolean isparked) {
this.isparked = isparked;
}

public Boolean getIsnocardinfo() {
return isnocardinfo;
}

public void setIsnocardinfo(Boolean isnocardinfo) {
this.isnocardinfo = isnocardinfo;
}

public Boolean getIsexpirydue() {
return isexpirydue;
}

public void setIsexpirydue(Boolean isexpirydue) {
this.isexpirydue = isexpirydue;
}

public Boolean getIschargefailed() {
return ischargefailed;
}

public void setIschargefailed(Boolean ischargefailed) {
this.ischargefailed = ischargefailed;
}

public Boolean getIsgiftredeem() {
return isgiftredeem;
}

public void setIsgiftredeem(Boolean isgiftredeem) {
this.isgiftredeem = isgiftredeem;
}

public Boolean getIscampaignredeem() {
return iscampaignredeem;
}

public void setIscampaignredeem(Boolean iscampaignredeem) {
this.iscampaignredeem = iscampaignredeem;
}

public Boolean getIsfixedcampredeem() {
return isfixedcampredeem;
}

public void setIsfixedcampredeem(Boolean isfixedcampredeem) {
this.isfixedcampredeem = isfixedcampredeem;
}

public Boolean getIsdiscountcampredeem() {
return isdiscountcampredeem;
}

public void setIsdiscountcampredeem(Boolean isdiscountcampredeem) {
this.isdiscountcampredeem = isdiscountcampredeem;
}

public Boolean getIscardupdate() {
return iscardupdate;
}

public void setIscardupdate(Boolean iscardupdate) {
this.iscardupdate = iscardupdate;
}

public Boolean getIspurchase() {
return ispurchase;
}

public void setIspurchase(Boolean ispurchase) {
this.ispurchase = ispurchase;
}

}