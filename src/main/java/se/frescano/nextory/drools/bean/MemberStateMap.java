package se.frescano.nextory.drools.bean;



public class MemberStateMap {


private String memberstate;
private Boolean isactivemember;
private Boolean isoldmember;
private Boolean isfreegroup;
private Boolean ispayinggroup;
private Boolean iscampaigngroup;
private Boolean isvisitorgroup;
private Boolean isvisitor;
private Boolean isvisitorgiftcard;
private Boolean isfreetrial;
private Boolean isfreecampaign;
private Boolean isfreegiftcard;
private Boolean ismemberpaying;
private Boolean ismembercampaign;
private Boolean ismembergiftcard;
private Boolean isfixedcampaign;
private Boolean isdiscountcampaign;
private Boolean isnonmember;
private Boolean isemployee;

public Boolean getIsemployee() {
	return isemployee;
}
public void setIsemployee(Boolean isemployee) {
	this.isemployee = isemployee;
}
public String getMemberstate() {
	return memberstate;
}
public void setMemberstate(String memberstate) {
	this.memberstate = memberstate;
}
public Boolean getIsactivemember() {
	return isactivemember;
}
public void setIsactivemember(Boolean isactivemember) {
	this.isactivemember = isactivemember;
}
public Boolean getIsoldmember() {
	return isoldmember;
}
public void setIsoldmember(Boolean isoldmember) {
	this.isoldmember = isoldmember;
}
public Boolean getIsfreegroup() {
	return isfreegroup;
}
public void setIsfreegroup(Boolean isfreegroup) {
	this.isfreegroup = isfreegroup;
}
public Boolean getIspayinggroup() {
	return ispayinggroup;
}
public void setIspayinggroup(Boolean ispayinggroup) {
	this.ispayinggroup = ispayinggroup;
}
public Boolean getIscampaigngroup() {
	return iscampaigngroup;
}
public void setIscampaigngroup(Boolean iscampaigngroup) {
	this.iscampaigngroup = iscampaigngroup;
}
public Boolean getIsvisitorgroup() {
	return isvisitorgroup;
}
public void setIsvisitorgroup(Boolean isvisitorgroup) {
	this.isvisitorgroup = isvisitorgroup;
}
public Boolean getIsvisitor() {
	return isvisitor;
}
public void setIsvisitor(Boolean isvisitor) {
	this.isvisitor = isvisitor;
}
public Boolean getIsvisitorgiftcard() {
	return isvisitorgiftcard;
}
public void setIsvisitorgiftcard(Boolean isvisitorgiftcard) {
	this.isvisitorgiftcard = isvisitorgiftcard;
}
public Boolean getIsfreetrial() {
	return isfreetrial;
}
public void setIsfreetrial(Boolean isfreetrial) {
	this.isfreetrial = isfreetrial;
}
public Boolean getIsfreecampaign() {
	return isfreecampaign;
}
public void setIsfreecampaign(Boolean isfreecampaign) {
	this.isfreecampaign = isfreecampaign;
}
public Boolean getIsfreegiftcard() {
	return isfreegiftcard;
}
public void setIsfreegiftcard(Boolean isfreegiftcard) {
	this.isfreegiftcard = isfreegiftcard;
}
public Boolean getIsmemberpaying() {
	return ismemberpaying;
}
public void setIsmemberpaying(Boolean ismemberpaying) {
	this.ismemberpaying = ismemberpaying;
}
public Boolean getIsmembercampaign() {
	return ismembercampaign;
}
public void setIsmembercampaign(Boolean ismembercampaign) {
	this.ismembercampaign = ismembercampaign;
}
public Boolean getIsmembergiftcard() {
	return ismembergiftcard;
}
public void setIsmembergiftcard(Boolean ismembergiftcard) {
	this.ismembergiftcard = ismembergiftcard;
}
public Boolean getIsfixedcampaign() {
	return isfixedcampaign;
}
public void setIsfixedcampaign(Boolean isfixedcampaign) {
	this.isfixedcampaign = isfixedcampaign;
}
public Boolean getIsdiscountcampaign() {
	return isdiscountcampaign;
}
public void setIsdiscountcampaign(Boolean isdiscountcampaign) {
	this.isdiscountcampaign = isdiscountcampaign;
}
public Boolean getIsnonmember() {
	return isnonmember;
}
public void setIsnonmember(Boolean isnonmember) {
	this.isnonmember = isnonmember;
}


}