package se.frescano.nextory.drools.bean;

public class TransitionResponse {
	
	private String newCurrentState;
	private String newPreviousState;
	private String newUserEvent;
	private String newSystemEvent;
	
	
	private Boolean error;
	private String errorMessage;
	
	
	public Boolean isError() {
		return error;
	}
	public void setError(Boolean error) {
		this.error = error;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getNewCurrentState() {
		return newCurrentState;
	}
	public void setNewCurrentState(String newCurrentState) {
		this.newCurrentState = newCurrentState;
	}
	public String getNewPreviousState() {
		return newPreviousState;
	}
	public void setNewPreviousState(String newPreviousState) {
		this.newPreviousState = newPreviousState;
	}
	public String getNewUserEvent() {
		return newUserEvent;
	}
	public void setNewUserEvent(String newUserEvent) {
		this.newUserEvent = newUserEvent;
	}
	public String getNewSystemEvent() {
		return newSystemEvent;
	}
	public void setNewSystemEvent(String newSystemEvent) {
		this.newSystemEvent = newSystemEvent;
	}
}

