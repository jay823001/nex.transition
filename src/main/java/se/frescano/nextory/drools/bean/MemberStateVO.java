package se.frescano.nextory.drools.bean;

public class MemberStateVO {
	private String previousState;
	private String currentState;	
    private String userEvent;
    private String systemEvent;
	private String trigger;
	private String	result;
    private String newPreviousState;
    private String newCurrentState;
    private String newUserEvent;
    private String newSystemEvent;
    
    private Integer    rulesExecutedCount;

	public String getUserEvent() {
		return userEvent;
	}
	public void setUserEvent(String userEvent) {
		this.userEvent = userEvent;
	}
	public String getSystemEvent() {
		return systemEvent;
	}
	public void setSystemEvent(String systemEvent) {
		this.systemEvent = systemEvent;
	}
	public String getNewPreviousState() {
		return newPreviousState;
	}
	public void setNewPreviousState(String newPreviousState) {
		this.newPreviousState = newPreviousState;
	}
	public String getNewCurrentState() {
		return newCurrentState;
	}
	public void setNewCurrentState(String newCurrentState) {
		this.newCurrentState = newCurrentState;
	}
	public String getNewUserEvent() {
		return newUserEvent;
	}
	public void setNewUserEvent(String newUserEvent) {
		this.newUserEvent = newUserEvent;
	}
	public String getNewSystemEvent() {
		return newSystemEvent;
	}
	public void setNewSystemEvent(String newSystemEvent) {
		this.newSystemEvent = newSystemEvent;
	}
	public Integer getRulesExecutedCount() {
		return rulesExecutedCount;
	}
	public void setRulesExecutedCount(Integer rulesExecutedCount) {
		this.rulesExecutedCount = rulesExecutedCount;
	}
	public String getPreviousState() {
		return previousState;
	}
	public void setPreviousState(String previousState) {
		this.previousState = previousState;
	}
	public String getCurrentState() {
		return currentState;
	}
	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}
	
	public String getTrigger() {
		return trigger;
	}
	public void setTrigger(String trigger) {
		this.trigger = trigger;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	
	
	
	

}
