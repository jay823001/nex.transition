package se.frescano.nextory.member.transition.constants;

import org.slf4j.MDC;

import datadog.trace.api.CorrelationIdentifier;

public class CONSTANTS {

	public static String TRANSITION_REQUEST ="/transitionrequest";
	public static String EVENT_STATE_MAP_REQUEST ="/eventstatemap";
	public static String MEMBER_STATE_MAP_REQUEST ="/memberstatemap";
	public static String ACTIVE_STATES_REQUEST="/activestates";
	public static String INACTIVE_STATES_REQUEST="/inactivestates";
	public static String DIRECT_TERMINATE_STATES_REQUEST="/directterminatestates";
	public static String CAMPAIGN_GROUP_STATES_REQUEST="/campaigngroupstates";
	
	public static void setDDTraceIds(){
		try{
			MDC.put("ddTraceID", "ddTraceID:" + String.valueOf(CorrelationIdentifier.getTraceId()));
		    MDC.put("ddSpanID", "ddSpanID:" + String.valueOf(CorrelationIdentifier.getSpanId()));		    
		}catch(Exception e){
			System.out.println("Error occured for tracing");
		}
	}
}
