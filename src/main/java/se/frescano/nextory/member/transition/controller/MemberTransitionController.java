package se.frescano.nextory.member.transition.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import se.frescano.nextory.drools.bean.EventStateMap;
import se.frescano.nextory.drools.bean.MemberStateEnum;
import se.frescano.nextory.drools.bean.MemberStateMap;
import se.frescano.nextory.drools.bean.MemberStateVO;
import se.frescano.nextory.drools.bean.SystemEvent;
import se.frescano.nextory.drools.bean.TransitionResponse;
import se.frescano.nextory.drools.bean.UserEvent;
import se.frescano.nextory.drools.util.NextoryStateTransitionEngine;
import se.frescano.nextory.member.transition.constants.CONSTANTS;

@RestController
@RequestMapping("/api/mt/{version:1.0}")
public class MemberTransitionController {
	
	@Autowired
	private NextoryStateTransitionEngine nextoryStateTransitionEngine;
	private	ObjectMapper mapper = new ObjectMapper();
	


	private static final Logger logger = LoggerFactory.getLogger(MemberTransitionController.class);

	
	@ApiOperation(value = "Do the member transition", nickname = "transitionrequest", notes = "This API takes the facts and do the member transition",
			response = TransitionResponse.class, tags={  })
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid request Object"),
	@ApiResponse(code = 404, message = "Transition Server Down"),
	@ApiResponse(code = 200, message = "Successful response", response = TransitionResponse.class) })
	@RequestMapping(value = "/transitionrequest", method = RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody TransitionResponse memberShipTransitionChangeRequest(@ApiParam(name="TRANSITION REQUEST", value="The Request contains Transition Request with Facts", required=true)	@RequestBody String transReqStrring){
		CONSTANTS.setDDTraceIds();
		TransitionResponse transitionResponse = null;
		 
		try {
			MemberStateVO memberStateVO= parseTransitionRequest(transReqStrring);
			memberStateVO = nextoryStateTransitionEngine.executeTransitionRules(memberStateVO);
			if(memberStateVO.getRulesExecutedCount() > 0){
				transitionResponse = new TransitionResponse();	
				transitionResponse.setNewPreviousState(memberStateVO.getNewPreviousState());
				transitionResponse.setNewCurrentState(memberStateVO.getNewCurrentState());
				transitionResponse.setNewSystemEvent(memberStateVO.getNewSystemEvent());
				transitionResponse.setNewUserEvent(memberStateVO.getNewUserEvent());
				transitionResponse.setError(false);
			}		
		}catch (Exception e) {
			logger.error("Error While doing the state transition"+e.getMessage());
			transitionResponse.setError(true);
			transitionResponse.setErrorMessage(e.getMessage());
		}		
		MDC.clear();
		return transitionResponse;
	}

	
		
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid request Object"),
	@ApiResponse(code = 404, message = "Transition Server Down"),
	@ApiResponse(code = 200, message = "Successful response", response = TransitionResponse.class) })
	@RequestMapping(value = "/memberstatemap", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String , MemberStateMap> memberstatemap(){
		CONSTANTS.setDDTraceIds();
		Map<String , MemberStateMap> memberstatemap = new HashMap<>();
		for(MemberStateEnum state : MemberStateEnum.values()){
			MemberStateMap memberState = new MemberStateMap();
			memberState.setMemberstate(state.name());
			memberState.setIsactivemember(state.isActiveMember());
			memberState.setIsoldmember(state.isOldMember());
			memberState.setIsfreegroup(state.isFreeMemberGroup());
			memberState.setIspayinggroup(state.isPayingMemberGroup());
			memberState.setIscampaigngroup(MemberStateEnum.isCampaignGroup(state.name()));
			memberState.setIsvisitorgroup(MemberStateEnum.isVisitorGroup(state.name()));
			memberState.setIsvisitor((MemberStateEnum.VISITOR == state)?true:false);
			memberState.setIsvisitorgiftcard((MemberStateEnum.VISITOR_GIFTCARD == state)?true:false);
			memberState.setIsfreetrial((MemberStateEnum.FREE_TRIAL == state)?true:false);
			memberState.setIsfreecampaign((MemberStateEnum.FREE_CAMPAIGN == state)?true:false);
			memberState.setIsfreegiftcard((MemberStateEnum.FREE_GIFTCARD == state)?true:false);
			memberState.setIsmemberpaying((MemberStateEnum.MEMBER_PAYING == state)?true:false);
			memberState.setIsmembercampaign((MemberStateEnum.MEMBER_CAMPAIGN == state)?true:false);
			memberState.setIsmembergiftcard((MemberStateEnum.MEMBER_GIFTCARD == state)?true:false);
			memberState.setIsfixedcampaign((MemberStateEnum.MEMBER_FIXEDCAMPAIGN == state)?true:false);
			memberState.setIsdiscountcampaign((MemberStateEnum.MEMBER_DISCOUNTCAMPAIGN == state)?true:false);
			memberState.setIsnonmember((MemberStateEnum.NONMEMBER == state)?true:false);
			memberState.setIsemployee((MemberStateEnum.EMPLOYEE == state)?true:false);
			memberstatemap.put(state.name(), memberState);
		}
		MDC.clear();
			return memberstatemap;
	}
	
	
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid request Object"),
	@ApiResponse(code = 404, message = "Transition Server Down"),
	@ApiResponse(code = 200, message = "Successful response", response = TransitionResponse.class) })
	@RequestMapping(value = "/eventstatemap", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String , EventStateMap> eventStates(){
		CONSTANTS.setDDTraceIds();
		Map<String , EventStateMap> eventsMap = new HashMap<>();
		for(UserEvent userevent : UserEvent.values()){			
				EventStateMap eventState = new EventStateMap();
				eventState.setEventname(userevent.name());
				eventState.setIsuserevent(true);
				eventState.setIssystemevent(false);
				eventState.setIscancelled((UserEvent.CANCELLED == userevent)?true:false);
				eventState.setIschargefailed((UserEvent.CHARGE == userevent)?true:false);
				eventState.setIsgiftredeem((UserEvent.G_REDEEM == userevent)?true:false);
				eventState.setIscampaignredeem((UserEvent.C_REDEEM == userevent)?true:false);
				eventState.setIsfixedcampredeem((UserEvent.F_REDEEM == userevent)?true:false);
				eventState.setIsdiscountcampredeem((UserEvent.D_REDEEM == userevent)?true:false);
				eventState.setIscardupdate((UserEvent.CARDUPDATE == userevent)?true:false);
				eventState.setIspurchase((UserEvent.PURCHASE == userevent)?true:false);
				eventState.setIsnoevent((UserEvent.NA == userevent)?true:false);
				eventState.setIsparked(false);
				eventState.setIsnocardinfo(false);
				eventState.setIsexpirydue(false);
				eventsMap.put(userevent.name(), eventState);			
		}		
		for(SystemEvent sysEvent : SystemEvent.values()){
			if(sysEvent != SystemEvent.NA){
				EventStateMap eventState = new EventStateMap();
				eventState.setEventname(sysEvent.name());
				eventState.setIsuserevent(false);
				eventState.setIssystemevent(true);
				eventState.setIscancelled(false);
				eventState.setIschargefailed(false);
				eventState.setIsgiftredeem(false);
				eventState.setIscampaignredeem(false);
				eventState.setIsfixedcampredeem(false);
				eventState.setIsdiscountcampredeem(false);
				eventState.setIscardupdate(false);
				eventState.setIspurchase(false);
				eventState.setIsparked((SystemEvent.PARKED == sysEvent)?true:false);
				eventState.setIsnocardinfo((SystemEvent.NOCARDINFO == sysEvent)?true:false);
				eventState.setIsexpirydue((SystemEvent.EXPIRYDUE == sysEvent)?true:false);
				eventState.setIsnoevent(false);
				eventsMap.put(sysEvent.name(), eventState);
			}			
		}		
		MDC.clear();
		return eventsMap;
	}

	
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid request Object"),
	@ApiResponse(code = 404, message = "Transition Server Down"),
	@ApiResponse(code = 200, message = "Successful response", response = TransitionResponse.class) })
	@RequestMapping(value = "/activestates", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<String> activestates(){
		CONSTANTS.setDDTraceIds();
		List<String> lstActiveState = new ArrayList<>();
		for(MemberStateEnum state : MemberStateEnum.values()){
			 if(state.isActiveMember()) lstActiveState.add(state.name());
		}
		MDC.clear();		
		return lstActiveState;
	}
	
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid request Object"),
	@ApiResponse(code = 404, message = "Transition Server Down"),
	@ApiResponse(code = 200, message = "Successful response", response = TransitionResponse.class) })
	@RequestMapping(value = "/inactivestates", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<String> inactivestates(){
		CONSTANTS.setDDTraceIds();
		List<String> lstInactiveState = new ArrayList<>();
		for(MemberStateEnum state : MemberStateEnum.values()){
			 if(!state.isActiveMember())  lstInactiveState.add(state.name());
		}
		MDC.clear();
		return lstInactiveState;
	}
	
	
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid request Object"),
	@ApiResponse(code = 404, message = "Transition Server Down"),
	@ApiResponse(code = 200, message = "Successful response", response = TransitionResponse.class) })
	@RequestMapping(value = "/directterminatestates", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<String> directterminatestates(){
		CONSTANTS.setDDTraceIds();
		List<String> lstDirectTerminateStates = new ArrayList<>();
		lstDirectTerminateStates.add(MemberStateEnum.FREE_TRIAL.name());
		lstDirectTerminateStates.add(MemberStateEnum.FREE_CAMPAIGN.name());
		MDC.clear();
		return lstDirectTerminateStates;
	}
	
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid request Object"),
	@ApiResponse(code = 404, message = "Transition Server Down"),
	@ApiResponse(code = 200, message = "Successful response", response = TransitionResponse.class) })
	@RequestMapping(value = "/campaigngroupstates", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<String> campaigngroupstates(){
		CONSTANTS.setDDTraceIds();
		List<String> lstCampaigngroupstates = new ArrayList<>();
		lstCampaigngroupstates.add(MemberStateEnum.FREE_CAMPAIGN.name());
		lstCampaigngroupstates.add(MemberStateEnum.MEMBER_CAMPAIGN.name());
		lstCampaigngroupstates.add(MemberStateEnum.MEMBER_FIXEDCAMPAIGN.name());
		lstCampaigngroupstates.add(MemberStateEnum.MEMBER_DISCOUNTCAMPAIGN.name());
		MDC.clear();		
		return lstCampaigngroupstates;
	}
	
	private MemberStateVO parseTransitionRequest(String transReqStrring) throws Exception{		
		if( logger.isTraceEnabled())logger.trace("Transition request recevied--->" + transReqStrring );
		MemberStateVO stateVo	= null;
		try{
			JsonNode 	node = mapper.readTree(transReqStrring);
			stateVo	= new MemberStateVO();
			stateVo.setPreviousState(mapper.convertValue(node.get("previousState"), String.class));
			stateVo.setCurrentState(mapper.convertValue(node.get("currentState"), String.class));
			stateVo.setUserEvent(mapper.convertValue(node.get("userEvent"), String.class));
			stateVo.setSystemEvent(mapper.convertValue(node.get("systemEvent"), String.class));
			stateVo.setTrigger(mapper.convertValue(node.get("trigger"), String.class));
			stateVo.setResult(mapper.convertValue(node.get("result"), String.class));

		}catch(Exception e){
			logger.error("Error while parsing transition request..::: " + transReqStrring + ".. :" + e.getMessage());
			throw new Exception(e.getMessage());
		}
		return stateVo;
	}
	

	@ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid request Object"),
	@ApiResponse(code = 404, message = "Transition Server Down"),
	@ApiResponse(code = 200, message = "Successful response", response = TransitionResponse.class) })
	@RequestMapping(value = "/allstates", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<String> memberActiveStates(){
		CONSTANTS.setDDTraceIds();
		List<String> allState = new ArrayList<>();
		for(MemberStateEnum state : MemberStateEnum.values()){
			allState.add(state.name());
		}
		MDC.clear();		
		return allState;
	}
	

	
}
