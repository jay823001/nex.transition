package se.frescano.nextory.member.transition.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.frescano.nextory.drools.bean.MemberStateEnum;
import se.frescano.nextory.drools.bean.MemberStateVO;
import se.frescano.nextory.drools.bean.SystemEvent;
import se.frescano.nextory.drools.bean.TransitionResponse;
import se.frescano.nextory.drools.bean.TriggerEnum;
import se.frescano.nextory.drools.bean.UserEvent;
import se.frescano.nextory.drools.util.NextoryStateTransitionEngine;

@Controller
public class HealthCheckController {

	public static final Logger _logger = LoggerFactory.getLogger(HealthCheckController.class);
	@Autowired
	private NextoryStateTransitionEngine nextoryStateTransitionEngine;

	
	@RequestMapping( value = "/howami", method = RequestMethod.GET)
	public ModelAndView checkSystem(HttpServletRequest req, HttpServletResponse res){						
		//_logger.info("HealthCheckController....checkSystem....Start");
		ModelAndView	mv = null;
		TransitionResponse transitionResponse=null;
		try {
			MemberStateVO memberStateVO= new MemberStateVO();
			memberStateVO.setPreviousState(MemberStateEnum.VISITOR.name());
			memberStateVO.setCurrentState(MemberStateEnum.VISITOR.name());
			memberStateVO.setUserEvent(UserEvent.NA.name());
			memberStateVO.setSystemEvent(SystemEvent.NA.name());
			memberStateVO.setTrigger(TriggerEnum.AUTHORIZATION.name());
			memberStateVO.setResult("SUCCESS");
			memberStateVO = nextoryStateTransitionEngine.executeTransitionRules(memberStateVO);
			if(memberStateVO.getRulesExecutedCount() > 0){
				transitionResponse = new TransitionResponse();	
				transitionResponse.setNewPreviousState(memberStateVO.getNewPreviousState());
				transitionResponse.setNewCurrentState(memberStateVO.getNewCurrentState());
				transitionResponse.setNewSystemEvent(memberStateVO.getNewSystemEvent());
				transitionResponse.setNewUserEvent(memberStateVO.getNewUserEvent());
				transitionResponse.setError(false);
				res.setHeader("X-Custom-Header", "alive");
				res.setHeader("X-App-Header", "nex-mt");			
				if( System.getenv("MY_POD_NAME") != null )
					res.setHeader("X-Host-Header", System.getenv("MY_POD_NAME"));
			}		
		}catch (Exception e) {
			_logger.error("Error While doing the state transition"+e.getMessage());
			transitionResponse.setError(true);
			transitionResponse.setErrorMessage(e.getMessage());
		}		
		
	//	_logger.info("HealthCheckController....checkSystem....End");
		return mv;
	}
}
