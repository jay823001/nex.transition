package se.frescano.nextory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableCaching
@EnableSwagger2
public class NexMemberTransitionApplication {
	
	private static final Logger logger = LoggerFactory.getLogger(NexMemberTransitionApplication.class);

	public static boolean 	isProductionEnv = false;
	public static String 	environment ;
	
	static ConfigurableApplicationContext _APPLICATIONCONTEXT;
	public static void main(String[] args) {		
		_APPLICATIONCONTEXT = SpringApplication.run(NexMemberTransitionApplication.class, args);
	}
	
	@Bean
	public Docket swaggerApi() {
		return new Docket(DocumentationType.SWAGGER_2)
	    .select()
	    .apis(RequestHandlerSelectors.basePackage("se.frescano"))
	    .paths(PathSelectors.regex("/api.*"))
	    .build()
	    .apiInfo(new ApiInfoBuilder().version("1.0").title("Member transition APIs").description("Documentation Member transition API v1.0").build());
	}
}
